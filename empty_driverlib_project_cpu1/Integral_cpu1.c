//#############################################################################
//
// FILE:   empty_driverlib_main_cpu1.c
//
// TITLE:  Empty Project
//
// CPU1 Empty Project Example
//
// This example is an empty project setup for Driverlib development for CPU1.
//
//#############################################################################
//
// $Release Date: $
// $Copyright:
// Copyright (C) 2013-2022 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//#############################################################################

//
// Included Files
//
#include "driverlib.h"
#include "device.h"

#include "constants/sine.h"

//
// Main
//

#define SIGNAL_FREQ 60
#define SAMPLE_RATE 3000
#define SAMPLES_PER_CYCLE   SAMPLE_RATE / SIGNAL_FREQ

#define WAVEFORM_BUFFER 150

signed int  integral_1[WAVEFORM_BUFFER];
signed int  integral_2[WAVEFORM_BUFFER];
signed int  integral_3[WAVEFORM_BUFFER];
signed int  integral_4[WAVEFORM_BUFFER];

int sample_counter = 0;

void main(void){

    while(1){

        if(sample_counter < WAVEFORM_BUFFER){
            sample_counter++;
            integral_1[sample_counter] = sine_lut[sample_counter % SINE_LUT_SAMPLES] + integral_1[sample_counter - 1];
            integral_2[sample_counter] = sine_lut[sample_counter % SINE_LUT_SAMPLES] + (integral_2[sample_counter - 1] >> 1);   //Devide by 2
            integral_3[sample_counter] = sine_lut[sample_counter % SINE_LUT_SAMPLES] + (integral_3[sample_counter - 1] << 1);   //Multiply by 2
            integral_4[sample_counter] = sine_lut[sample_counter % SINE_LUT_SAMPLES] + (integral_4[sample_counter - 1] / 2);    //Actual Div by 2
        }else{
            continue;
        }
    }
}

//
// End of File
//
