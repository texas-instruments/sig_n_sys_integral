/*
 * sine.h
 *
 *  Created on: Jun 14, 2023
 *      Author: brand
 */



#define SINE_LUT_SAMPLES    50

signed int sine_lut[SINE_LUT_SAMPLES] = {
    0,
    32,
    64,
    94,
    123,
    150,
    175,
    197,
    216,
    232,
    243,
    251,
    255,
    255,
    251,
    243,
    232,
    216,
    197,
    175,
    150,
    123,
    94,
    64,
    32,
    0,
    -32,
    -64,
    -94,
    -123,
    -150,
    -175,
    -197,
    -216,
    -232,
    -243,
    -251,
    -255,
    -255,
    -251,
    -243,
    -232,
    -216,
    -197,
    -175,
    -150,
    -123,
    -94,
    -64,
    -32,
};


#ifndef CONSTANTS_SINE_H_
#define CONSTANTS_SINE_H_

#endif /* CONSTANTS_SINE_H_ */
